/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable react/jsx-key */
import React, { useEffect, useState } from "react";
import { Button, Platform, StyleSheet, Text, View } from "react-native";
import SearchInput, { createFilter } from "react-native-search-filter";
import useSafeSetState from "../utils/useSafeState";
import { JsonForm } from "./components/json-form/JsonForm";

export default function BookingListingAdmin({ navigation }: any) {
  const [_schema, setSchema] = useSafeSetState({
    type: "object",
    properties: {
      actions: {
        enum: ["Approve", "Cancel"],
        type: "string",
      },
    },
  });

  const _uiSchema = {
    actions: {
      "ui:title": "Actions",
      "ui:placeholder": "Select",
      "ui:widget": "select",
    },
  };

  const [searchItem, setSearchItem] = useState("");
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);

  const filterData = data.filter(
    createFilter(searchItem, ["name", "body", "date", "time"])
  );

  useEffect(() => {
    setLoading(true);
    const fetchData = async () => {
      // TODO: change endpoint with original one.
      // TODO: get data accoring to who is logged in
      const res = await fetch(
        "https://run.mocky.io/v3/27980060-6ba6-4023-a642-336bb42975ae"
      );
      const resJSON = await res.json();
      setData(resJSON.bookinglistingAdmin);
      setLoading(false);
    };
    fetchData();
  }, []);

  if (loading) {
    return <Text>Loading...</Text>;
  }

  return (
    <View>
      <View style={{ flexDirection: "row", justifyContent: "space-around" }}>
        <SearchInput
          placeholder="Enter Keyword to Search"
          onChangeText={(value) => setSearchItem(value)}
          style={{ padding: 5 }}
        />
      </View>
      <View style={{ margin: 10 }}>
        {data.length ? (
          <View
            style={{
              flexDirection: "row",
              borderBottomWidth: 2,
              borderBottomColor: "grey",
              paddingVertical: 10,
              marginLeft: 30,
              marginRight: 5,
            }}
          >
            <Text style={{ flex: 5, justifyContent: "center" }}>Name</Text>
            <Text style={{ flex: 4, justifyContent: "center" }}>Facility</Text>
            <Text style={{ flex: 3, justifyContent: "center" }}>Date</Text>
            <Text style={{ flex: 3, justifyContent: "center" }}>Time</Text>
          </View>
        ) : null}
        {filterData.map((d) => {
          return (
            <View
              style={{
                flexDirection: "row",
                paddingVertical: 10,
                paddingRight: 5,
                marginLeft: 30,
              }}
            >
              <Text style={{ flex: 5 }}>{d.name}</Text>
              {Platform.OS === "web" ? (
                <Text style={{ flex: 4 }}>{d.body}</Text>
              ) : (
                <Text style={{ flex: 3 }}>{d.body}</Text>
              )}

              {Platform.OS === "web" ? (
                <Text style={{ flex: 3 }}>{d.date}</Text>
              ) : (
                <Text style={{ flex: 4 }}>{d.date}</Text>
              )}
              <Text style={{ flex: 3 }}>{d.time}</Text>
            </View>
          );
        })}
        <JsonForm
          schema={_schema}
          uiSchema={_uiSchema}
          _onSuccess={(e) => {
            navigation.navigate("AdminConfirmation");
          }}
        />
        <View style={styles.Btns}>
          <Button
            title="Apply"
            onPress={() => navigation.navigate("AdminConfirmation")}
          />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  Btns: {
    padding: 20,
    width: 200,
    marginHorizontal: 110,
  },
});
