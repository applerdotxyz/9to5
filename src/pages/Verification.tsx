/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable react/prop-types */
import React from "react";
import { Button, Platform, View } from "react-native";
import useSafeSetState from "../utils/useSafeState";
import { BarcodeScan } from "./BarcodeScan";
import { JsonForm } from "./components/json-form/JsonForm";

export const Verification = (props) => {
  const [_schema, setSchema] = useSafeSetState({
    type: "object",
    properties: {
      pnr: { type: "string" },
    },
  });

  const _uiSchema = {
    pnr: {
      "ui:title": "Enter PNR",
    },
  };

  return (
    <>
      {Platform.OS === "web" ? (
        <View>
          <JsonForm
            schema={_schema}
            uiSchema={_uiSchema}
            _onSuccess={(e) => {
              props.navigation.navigate("ReBooking");
            }}
          />
          <Button
            title="Verify"
            onPress={() => {
              props.navigation.navigate("ReBooking");
            }}
          />
        </View>
      ) : (
        <>
          <BarcodeScan />
          <Button
            title="Verify"
            onPress={() => {
              props.navigation.navigate("ReBooking");
            }}
          />
        </>
      )}
    </>
  );
};
