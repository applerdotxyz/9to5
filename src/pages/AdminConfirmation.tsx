/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable react/prop-types */
import React, { useState } from "react";
import {
  Button,
  FlatList,
  Image,
  Text,
  TouchableOpacity,
  View,
  StyleSheet,
} from "react-native";
import useSafeSetState from "../utils/useSafeState";
import { JsonForm } from "./components/json-form/JsonForm";

const DATA = [
  {
    id: "1",
    description: "PNR :825084\n ",
  },
];

const Item = ({ item, onPress, style }) => (
  <TouchableOpacity onPress={onPress} style={[styles.item, style]}>
    <Text style={styles.description}>{item.description}</Text>
  </TouchableOpacity>
);

export const AdminConfirmation = (props: {
  navigation: { navigate: (arg0: string) => void };
}) => {
  const [_schema, setSchema] = useSafeSetState({
    type: "object",
    properties: {
      actions: {
        enum: ["Approve", "Cancel"],
        type: "string",
      },
    },
  });

  const _uiSchema = {
    actions: {
      "ui:title": "Change Status",
      "ui:placeholder": "Select",
      "ui:widget": "select",
    },
  };

  const [selectedId, setSelectedId] = useState(null);
  const qrUrl =
    "https://www.qr-code-generator.com/wp-content/themes/qr/new_structure/markets/core_market_full/generator/dist/generator/assets/images/websiteQRCode_noFrame.png";

  const renderItem = ({ item }) => {
    const backgroundColor = item.id === selectedId ? "yellow" : "white";

    return (
      <Item
        item={item}
        onPress={() => setSelectedId(item.id)}
        style={{ backgroundColor }}
      />
    );
  };

  return (
    <>
      <View style={{ justifyContent: "center", alignItems: "center" }}>
        <Image
          source={{
            uri: qrUrl,
          }}
          style={{ height: 400, width: 300 }}
        />
        <FlatList
          data={DATA}
          renderItem={renderItem}
          keyExtractor={(item) => item.id}
          extraData={selectedId}
        />
        <Button
          title="Verify"
          onPress={() => {
            props.navigation.navigate("Verification");
          }}
        />
      </View>
      <JsonForm
        schema={_schema}
        uiSchema={_uiSchema}
        _onSuccess={(_e: any) => {
          props.navigation.navigate("ReBooking");
        }}
      />
      <View style={styles.Btns}>
        <Button
          title="Create New Booking"
          onPress={() => {
            props.navigation.navigate("ReBooking");
          }}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  item: {
    padding: 2,
    marginVertical: 6,
    marginHorizontal: 8,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "#D3D6D6",
    backgroundColor: "#FFFFFF",
    opacity: 1,
    borderRadius: 2,
  },
  description: {
    fontSize: 18,
    color: "red",
    textAlign: "left",
  },
  Btns: {
    padding: 20,
    width: 200,
    marginHorizontal: 110,
  },
});
