/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable react/prop-types */
import React from "react";
import { Button, StyleSheet, View } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import useSafeSetState from "../utils/useSafeState";
import { JsonForm } from "./components/json-form/JsonForm";

export const TimeSlot = (props) => {
  const [_schema, setSchema] = useSafeSetState({
    type: "object",
    properties: {
      slots: {
        type: "string",
        enum: ["1pm-2pm", "2pm-3pm", "3pm-4pm"],
      },
    },
  });

  const _uiSchema = {
    slots: {
      "ui:title": "Please select your slot",
      "ui:widget": "radioboxes",
    },
  };

  return (
    <ScrollView>
      <View style={{ justifyContent: "center", flex: 1, alignItems: "center" }}>
        <JsonForm
          schema={_schema}
          uiSchema={_uiSchema}
          _onSuccess={(e) => {
            props.navigation.navigate("Add");
          }}
        />
        <View style={styles.Btns}>
          <Button
            title="Continue"
            onPress={() => {
              props.navigation.navigate("Add");
            }}
          />
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  Btns: {
    padding: 20,
    width: 200,
    marginHorizontal: 110,
  },
});
