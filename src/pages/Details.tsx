/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable react/prop-types */
import React, { useState } from "react";
import {
  Button,
  FlatList,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";

const DATA = [
  {
    id: "1",
    title: "Details\n\n\n",
    description:
      "We provide service for music sessions.\n\nYou will learn here:\nClassical\nPop\n\n",
    subInfo: " 2 batches a week\n\nTimings\n\n1pm-2pm\n2pm-3pm\n ",
  },
];

const Item = ({ item, onPress, style }) => (
  <TouchableOpacity onPress={onPress} style={[styles.item, style]}>
    <Text style={styles.title}>{item.title}</Text>
    <Text style={styles.description}>{item.description}</Text>
    <Text style={styles.subInfo}>{item.subInfo}</Text>
  </TouchableOpacity>
);

export const Details = (props) => {
  const [selectedId, setSelectedId] = useState(null);

  const renderItem = ({ item }) => {
    const backgroundColor = item.id === selectedId ? "yellow" : "white";

    return (
      <Item
        item={item}
        onPress={() => setSelectedId(item.id)}
        style={{ backgroundColor }}
      />
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={{ justifyContent: "center", flex: 1 }}>
        <FlatList
          style={{ margin: 10 }}
          data={DATA}
          renderItem={renderItem}
          keyExtractor={(item) => item.id}
          extraData={selectedId}
        />
        <View style={styles.Btns}>
          <Button
            title="Book"
            onPress={() => {
              props.navigation.navigate("Cal");
            }}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
    minHeight: 30,
  },
  item: {
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "#D3D6D6",
    backgroundColor: "#FFFFFF",
    opacity: 1,
    borderRadius: 2,
  },
  title: {
    fontSize: 30,
    color: "black",
    fontWeight: "bold",
    textAlign: "center",
  },
  description: {
    fontSize: 18,
    color: "red",
    textAlign: "left",
  },
  subInfo: {
    fontSize: 15,
    color: "grey",
    textAlign: "center",
  },
  Btns: {
    padding: 20,
    height: 200,
    width: 200,
    marginHorizontal: 110,
  },
});
