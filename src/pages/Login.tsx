/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable react/prop-types */

import React from "react";
import { Button, ScrollView, StyleSheet, View } from "react-native";
import useSafeSetState from "../utils/useSafeState";
import { JsonForm } from "./components/json-form/JsonForm";

export const Login = (props) => {
  const _formData = {
    phone: 8654787549,
    otp: 654789,
  };

  const [_schema, setSchema] = useSafeSetState({
    type: "object",
    properties: {
      phone: { type: "number" },
      otp: { type: "number" },
    },
  });

  const uiSchema = {
    phone: {
      "ui:title": "Phone No. ",
    },
  };

  return (
    <ScrollView>
      <View style={{ justifyContent: "center", flex: 1, alignItems: "center" }}>
        <JsonForm
          style={{ margin: 20 }}
          schema={_schema}
          _formData={_formData}
          uiSchema={uiSchema}
          _onSuccess={(e) => {
            props.navigation.navigate("Details");
          }}
        />

        <View style={styles.Btns}>
          <Button
            title="Phone Login"
            onPress={() => alert("Phone Login pressed")}
          />
        </View>
        <View style={styles.Btns}>
          <Button
            title="Google Login"
            onPress={() => alert("Google Login pressed")}
          />
        </View>
        <View style={styles.Btns}>
          <Button
            title="Twitter Login"
            onPress={() => alert("Twitter Login pressed")}
          />
        </View>
        <View style={styles.Btns}>
          <Button
            title="Facebook Login"
            onPress={() => alert("Facebook Login pressed")}
          />
        </View>
        <View style={styles.Btns}>
          <Button
            title="Submit"
            onPress={() => {
              props.navigation.navigate("Details");
            }}
          />
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  Btns: {
    flexDirection: "column",
    padding: 10,
    width: 200,
  },
});
