/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable react/prop-types */
import React from "react";
import { Button, ScrollView, StyleSheet, View } from "react-native";
import useSafeSetState from "../utils/useSafeState";
import { JsonForm } from "./components/json-form/JsonForm";

export const Profile = (props) => {
  const tickbox = ["Need Accesories", "Need Payments"];

  const [_schema, setSchema] = useSafeSetState({
    description: "",
    properties: {
      number: {
        type: "number",
      },
      name: {
        title: "Name",
        type: "string",
      },
      stype: {
        enum: ["Musician", "Dancer", "Professor"],
        type: "string",
      },
      tickbox: {
        type: "array",
        items: {
          type: "string",
        },
      },
    },
    required: ["name", "number"],
    title: "Demo",
    type: "object",
  });

  const _uiSchema = {
    tickbox: {
      "ui:title": "Select as per your needs",
      "ui:options": {
        addable: false,
        orderable: false,
        removable: false,
        minimumNumberOfItems: tickbox.length,
      },
      items: {
        "ui:iterate": (i, { value }) =>
          // (
          //     checkBox(value,tickbox,i)
          // )
          //require above comment code for future use of checkbox customization
          ({
            "ui:title": false,
            "ui:widget": "checkbox",
            "ui:widgetProps": {
              text: tickbox[i],
              value: tickbox[i],
              checked: (value || []).includes(tickbox[i]),
            },
          }),
      },
    },
    number: {
      "ui:title": "Phone Number",
    },
    stype: {
      "ui:title": "Select Type",
      "ui:placeholder": "Please select type",
      "ui:widget": "select",
    },
    "ui:order": ["name", "number"],
  };

  //require this function for future customization checkbox
  /*
        const checkBox = (value: any, tickboxArr: string[], i: number) => {
            console.log(value);
            console.log(tickboxArr);
            console.log(i);
            // const arr = [];
            // for(const element of tickboxArr){
            return {
            "ui.title": false,
            "ui.widget": "checkbox",
            "ui.widgetProps": {
                text: tickboxArr[i],
                value: tickboxArr[i],
                checked: (tickboxArr || []).includes(value),
            },
            };
            // arr.push(obj);
            // }
            // return obj;
        };
  */

  return (
    <ScrollView>
      <View style={{ justifyContent: "center", flex: 1, alignItems: "center" }}>
        <JsonForm
          schema={_schema}
          uiSchema={_uiSchema}
          _onSuccess={(e) => {
            props.navigation.navigate("CalSlot");
          }}
        />
        <View style={styles.Btns}>
          <Button
            title="Save"
            onPress={() => {
              props.navigation.navigate("CalSlot");
            }}
          />
        </View>
      </View>
    </ScrollView>
  );
};
const styles = StyleSheet.create({
  Btns: {
    padding: 20,
    width: 200,
    marginHorizontal: 110,
  },
});
