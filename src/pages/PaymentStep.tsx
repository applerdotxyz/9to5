/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable react/prop-types */
import React from "react";
import { Button, View, StyleSheet } from "react-native";

export const PaymentStep = (props) => {
  return (
    <>
      <View
        style={{
          justifyContent: "space-around",
          flex: 1,
          alignItems: "center",
        }}
      >
        <View style={styles.Btns}>
          <Button
            title="Upi"
            onPress={() => {
              props.navigation.navigate("PreConfirmation");
            }}
          />
        </View>
        <View style={styles.Btns}>
          <Button
            title="Pay Later"
            onPress={() => {
              props.navigation.navigate("PreConfirmation");
            }}
          />
        </View>
        <View style={styles.Btns}>
          <Button
            title="Paytm"
            onPress={() => {
              props.navigation.navigate("PreConfirmation");
            }}
          />
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  Btns: {
    padding: 10,
    width: 200,
  },
});
